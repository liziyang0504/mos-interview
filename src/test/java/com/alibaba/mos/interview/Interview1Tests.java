package com.alibaba.mos.interview;

import com.alibaba.mos.api.ItemService;
import com.alibaba.mos.api.SkuSimpleReadService;
import com.alibaba.mos.data.ChannelInventoryDO;
import com.alibaba.mos.data.ItemDO;
import com.alibaba.mos.data.SkuDO;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@SpringBootTest
class Interview1Tests {

    @Autowired
    SkuSimpleReadService skuSimpleReadService;

    @Autowired
    ItemService itemService;

    private static Logger log = LoggerFactory.getLogger(Interview1Tests.class);

    /**
     * 试题1:
     * 在com.alibaba.mos.service.SkuSimpleReadServiceImpl中实现com.alibaba.mos.api.SkuSimpleReadService#loadSkus()
     * 从/resources/data/skus.xls读取数据并返回对应数据实体
     */
    @Test
    void readSkuDataFromExcelTest() {
        List<SkuDO> list = skuSimpleReadService.loadSkus();
        Assert.isTrue(CollectionUtils.isNotEmpty(list), "未能读取到sku数据列表");
        //log.info("skus.xls数据={}",list);
        System.out.println("skus.xls数据："+list);

    }

    /**
     * 试题2:
     * 计算以下统计值:
     * 1、价格在最中间 的skuId
     * 2、每个渠道库存量为前五的skuId列表 例如( miao:[1,2,3,4,5],tmall:[3,4,5,6,7],intime:[7,8,4,3,1]
     * 3、所有sku的总价值
     */
    @Test
    void statisticsDataTest() {
        List<SkuDO> list = skuSimpleReadService.loadSkus();
        Assert.isTrue(CollectionUtils.isNotEmpty(list), "未能读取到sku数据列表");
        //1、价格在最中间 的skuId
        List<BigDecimal> priceList = new ArrayList<>();
        for (SkuDO skuDO : list) {
            priceList.add(skuDO.getPrice());
        }
        Collections.sort(priceList);
        BigDecimal midPrice = priceList.get(priceList.size() / 2); //商品中间值
        log.info("商品中间价格为：" + midPrice);
        //用数组接收多个中间价格的的skuId
        List<String> ids = new ArrayList<>();
        for (SkuDO skuDO : list) {
            if (skuDO.getPrice().equals(midPrice)) {
                ids.add(skuDO.getId());
            }
        }
        log.info("价格在最中间 的skuId：" + ids);
        //2、每个渠道库存量为前五的skuId列表
        List<ChannelInventoryDO> cidList = new ArrayList<>();
        BigDecimal skuCount = new BigDecimal(0);
        for (int i = 0; i < list.size(); i++) {
            List<ChannelInventoryDO> DOList = list.get(i).getInventoryList();
            BigDecimal inventoryCount = DOList.stream().map(ChannelInventoryDO::getInventory).reduce(BigDecimal.ZERO, BigDecimal::add);
            skuCount = skuCount.add(inventoryCount.max(list.get(i).getPrice()));
            for (int ii = 0; ii < DOList.size(); ii++) {
                ChannelInventoryDO hannelInventoryDO = DOList.get(ii);
                hannelInventoryDO.setSkuId(list.get(i).getId());
                cidList.add(hannelInventoryDO);
            }
        }
        //3、所有sku的总价值
        log.info("所有sku的总价值={}", skuCount);
        //按照库存量降序
        cidList = cidList.stream().sorted(Comparator.comparing(ChannelInventoryDO::getInventory).reversed()).collect(Collectors.toList());
        //按渠道分组
        Map<String, List<ChannelInventoryDO>> cidMap = cidList.stream().collect(Collectors.groupingBy(ChannelInventoryDO::getChannelCode));
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, List<ChannelInventoryDO>> entry : cidMap.entrySet()) {
            String channelCode = entry.getKey();
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(channelCode + ":[");
            List<ChannelInventoryDO> cList = entry.getValue();
            cList = cList.stream().limit(5).collect(Collectors.toList());
            StringBuilder sbn = new StringBuilder();
            for (int ii = 0; ii < cList.size(); ii++) {
                if (sbn.length() > 0) {
                    sbn.append(",");
                }
                sbn.append(cList.get(ii).getSkuId());
            }
            sb.append(sbn.toString() + "]");
        }
        System.out.println("每个渠道库存量为前五的skuId列表="+sb);
        //log.info("每个渠道库存量为前五的skuId列表={}", sb.toString());
    }

    /**
     * 试题3:
     * 在com.alibaba.mos.service.ItemServiceImpl中实现com.alibaba.mos.service.ItemService#aggregationSkus(java.util.List)
     * 读取sku列表并聚合为商品, 聚合规则为：
     * 对于sku type为原始商品(ORIGIN)的, 按货号(artNo)聚合成ITEM
     * 对于sku type为数字化商品(DIGITAL)的, 按spuId聚合成ITEM
     * 聚合结果需要包含: item的最大价格、最小价格、sku列表及全渠道总库存
     */
    @Test
    void aggregationSkusTest() {
        List<SkuDO> list = skuSimpleReadService.loadSkus();
        Assert.isTrue(CollectionUtils.isNotEmpty(list), "未能读取到sku数据列表");
        List<ItemDO> items = itemService.aggregationSkus(list);
        //log.info("聚合数据={}",items);
        System.out.println("聚合数据：" +items);
        Assert.isTrue(CollectionUtils.isNotEmpty(items), "未能聚合商品列表");
    }

}
