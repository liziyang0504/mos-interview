/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package com.alibaba.mos.service;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.mos.api.SkuSimpleReadService;
import com.alibaba.mos.data.ChannelInventoryDO;
import com.alibaba.mos.data.SkuDO;
import com.sun.rowset.internal.Row;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author superchao
 * @version $Id: SkuSimpleReadServiceImpl.java, v 0.1 2019年10月28日 11:54 AM superchao Exp $
 */
@Service
public class SkuSimpleReadServiceImpl implements SkuSimpleReadService {
    private static Logger log = LoggerFactory.getLogger(SkuSimpleReadServiceImpl.class);
    @Override
    public List<SkuDO> loadSkus() {
        //TODO 在此实现从resource文件excel中加载sku的代码
        InputStream ins;
        Workbook rwb;
        List<SkuDO> list = new ArrayList<>();
        try {
            ins = this.getClass().getClassLoader().getResourceAsStream("data/skus.xls");
            if (null==ins){
                log.info("找不到指定文件");
            }
            rwb = Workbook.getWorkbook(ins);
            //获得第一个标签页
            Sheet sheet = rwb.getSheet(0);
            //获取总行数
            int totalRows = sheet.getRows();
            //第一行
            Cell[] tiltle = sheet.getRow(0);
            for (int i = 1; i < totalRows; i++) {
                Cell[] cells = sheet.getRow(i);
                SkuDO skuDO = new SkuDO();
                for (int j = 0; j < cells.length; j++) {
                    //标题
                    String title = tiltle[j].getContents();
                    //内容
                    String content = cells[j].getContents();
                    //如果excel标题大小写不确定可统一转换小写匹配
                    if ("id".equals(title.toLowerCase())) {
                        skuDO.setId(content);
                    } else if ("name".equals(title)) {
                        skuDO.setName(content);
                    } else if ("artNo".equals(title)) {
                        skuDO.setArtNo(content);
                    } else if ("spuId".equals(title)) {
                        skuDO.setSpuId(content);
                    } else if ("skuType".equals(title)) {
                        skuDO.setSkuType(content);
                    } else if ("price".equals(title)) {
                        skuDO.setPrice(new BigDecimal(content));
                    } else if ("inventorys".equals(title)) {
                        List<ChannelInventoryDO> inventorysList = new ArrayList<ChannelInventoryDO>();
                        inventorysList = JSONObject.parseArray(content, ChannelInventoryDO.class);
                        skuDO.setInventoryList(inventorysList);
                    }
                }
                list.add(skuDO);
            }
            //关闭文件流
            ins.close();
            rwb.close();
        } catch (Exception e) {
            log.info(e.getMessage());
            e.printStackTrace();
        }
        return list;
    }
}
