/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package com.alibaba.mos.service;

import com.alibaba.mos.api.ItemService;
import com.alibaba.mos.data.ChannelInventoryDO;
import com.alibaba.mos.data.ItemDO;
import com.alibaba.mos.data.SkuDO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author superchao
 * @version $Id: ItemServiceImpl.java, v 0.1 2019年10月28日 11:11 AM superchao Exp $
 */
@Service
public class ItemServiceImpl implements ItemService {

    @Override
    public List<ItemDO> aggregationSkus(List<SkuDO> skuList) {
        //TODO 在此实现聚合sku的代码
        /**
         * 试题3:
         * 在com.alibaba.mos.service.ItemServiceImpl中实现com.alibaba.mos.service.ItemService#aggregationSkus(java.util.List)
         * 读取sku列表并聚合为商品, 聚合规则为：
         * 对于sku type为原始商品(ORIGIN)的, 按货号(artNo)聚合成ITEM
         * 对于sku type为数字化商品(DIGITAL)的, 按spuId聚合成ITEM
         * 聚合结果需要包含: item的最大价格、最小价格、sku列表及全渠道总库存
         */
        List<ItemDO> result = new ArrayList<>();
        List<SkuDO> skuDOList = new ArrayList<>();
        //对于sku type分组
        Map<String, List<SkuDO>> skuMap = skuList.stream().collect(Collectors.groupingBy(SkuDO::getSkuType));
        for (Map.Entry<String, List<SkuDO>> entry : skuMap.entrySet()) {
            String skuType = entry.getKey();
            List<SkuDO> sList = entry.getValue();
            if ("ORIGIN".equals(skuType)) {
                //对于sku type为原始商品(ORIGIN)的, 按货号(artNo)聚合成ITEM
                Map<String, List<SkuDO>> artNoMap = sList.stream().collect(Collectors.groupingBy(SkuDO::getArtNo));
                for (List<SkuDO> vList : artNoMap.values()) {
                    result.add(getItemDO(vList, skuType));
                }
            } else if ("DIGITAL".equals(skuType)) {
                //对于sku type为数字化商品(DIGITAL)的, 按spuId聚合成ITEM
                Map<String, List<SkuDO>> spuIdMap = sList.stream().collect(Collectors.groupingBy(SkuDO::getSpuId));
                for (List<SkuDO> vList : spuIdMap.values()) {
                    result.add(getItemDO(vList, skuType));
                }
            }
        }
        return result;
    }

    private ItemDO getItemDO(List<SkuDO> vList, String skuType) {
        ItemDO itemDO = new ItemDO();
        Optional<SkuDO> min = vList.stream().min(Comparator.comparing(SkuDO::getPrice));
        Optional<SkuDO> max = vList.stream().max(Comparator.comparing(SkuDO::getPrice));
        //item的最大价格
        itemDO.setMinPrice(min.get().getPrice());
        //最小价格
        itemDO.setMaxPrice(max.get().getPrice());
        BigDecimal inventory = new BigDecimal(0);
        StringBuilder names = new StringBuilder();
        StringBuilder artNos = new StringBuilder();
        StringBuilder spuIds = new StringBuilder();
        List<String> skuIds = new ArrayList<>();
        for (int i = 0; i < vList.size(); i++) {
            if (i > 0) {
                names.append(",");
                artNos.append(",");
                spuIds.append(",");
            }
            names.append(vList.get(i).getName());
            spuIds.append(vList.get(i).getSpuId());
            artNos.append(vList.get(i).getArtNo());
            SkuDO skuDO = vList.get(i);
            skuIds.add(skuDO.getId());
            List<ChannelInventoryDO> iList = skuDO.getInventoryList();
            BigDecimal big = iList.stream().map(ChannelInventoryDO::getInventory).reduce(BigDecimal.ZERO, BigDecimal::add);
            inventory = inventory.add(big);
        }
        itemDO.setName(names.toString());
        itemDO.setArtNo(artNos.toString());
        itemDO.setSpuId(spuIds.toString());
        //全渠道总库存
        itemDO.setInventory(inventory);
        //sku列表
        itemDO.setSkuIds(skuIds);
        return itemDO;
    }

}
